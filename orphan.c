#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
  pid_t pid = fork();

  if (pid == -1){
    perror("Fail forking");
    return 1;
  }
  if (pid == 0){
    sleep(12);
    printf("Child hello\n");
  }else{
    printf("Child pid %d\n", pid);
    exit(0);
  }
}
