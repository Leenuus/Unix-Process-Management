## Process

### TODO

1. virtual console
2. PAM sessions
    - https://www.reddit.com/r/linuxquestions/comments/n6akxv/can_someone_please_explain_what_does_the_process/
    - https://duckduckgo.com/?q=what+is+sd-pam+&hps=1&start=1&ia=web
3. session leader, session, `setsid`, `umask`

### Process Basic

1. `orphan`, When a process's parent dies before it, it becomes `orphan`, which attaches to the `init` process with pid 1, always `systemd` then.
2. `zombie`, when a process spawns a child process, __without wait for its complete status__ with `wait` or `waitpid`, the child process won't get recycled by kernel, so that before the parent dies, it keeps defunct.
3. `daemon`
    - No Controlling Terminal, double `fork` technique
    - Discard `stdout`, `stdin`, `stderr`
    - IPC methods: `signals`, `unix domain socket` ...
    - In fact, C library provides a wrapper `int daemon(int nochdir, int noclose);`, to help to create daemon, see `man daemon`
    - pitfall: without controlling terminal, process's output with standard io library will be `full buffered`, so when using `printf` like functions, you have to manually `fflush` the buffer or use `write` do low level io.

### How Unix Boot

1. `init` program, often `systemd` these days, it is responsible for spawning basic daemons and jobs
2. `getty`, ask a control terminal from kernel. In fact, no matter you are using the native terminal or a desktop environment, `getty` will ask one for you. A desktop environment needs a `tty` too.
3. `login`, after getting a `tty`, `getty` spawns `login` to log user in the system. And if you are using a desktop environment, it step can be a `display manager` gets called by `getty`, and then calls the `login`

#### `systemd`

1. Install your local software in `/usr/local/bin`
2. Basic unit service file

```systemd

[Unit]
Description=<Description>

[Service]
ExecStart=<executable path>
Restart=on-failure # restart behavior
# systemd can also start a simple program without daemon logic inside
# to start it, set `Type` to `simple`
Type=forking # old-fashion forking type daemon; 
PIDFile=/tmp/my-daemon/pid # tell systemd where to find the pid of daemon

[Install]
WantedBy=multi-user.target
```


### Signals

Signals is a mechanism in which kernel talks to process, or process talks to process.

Signals and meaning list see `man 7 signal`

How to install signal handler in a C program, see `man sigaction`

How to block signals, see `man sigprocmask`

Snippet

```c

void sig_handler(int sig);

struct sigaction sa;
sa.sa_handler = sig_handler;
sigemptyset(&sa.sa_mask);
sa.sa_flags = SA_RESTART; // restart the function after executing the signal handler

sigaction(SIGTERM, &sa, NULL); // register a signal handler for specific signal

```

### File IO Syscall

1. `stat`, get insight of file
    - check whether a file exist
    - check its type with posix macro like `S_ISREG(stat.st_mode)`
2. `remove`, `unlink` regular file or `rmdir`
    - `unlink` only remove file
    - `rmdir` removes directory
3. `open` `mkdir` mode flag
    - read, write, access: create dir with `S_IRUSR | S_IWUSR | S_IEXEC`
    - read and write: create file with `S_IRUSR | S_IWUSR`


### IPC

1. signal, `kill`. Caution: `SIGSTOP` and `SIGKILL` can __not__ be ignored or caught. The former stop the program and the latter terminates it.
2. 
