#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#define mem_size 1024
const char share_mem_name[] = "/my_mem";

int main()

{

  char *addr;
  int fd;
  fd = shm_open(share_mem_name, O_RDONLY, 0600);

  if (fd == -1) {
    perror("Fail to open shared memory");
    exit(EXIT_FAILURE);
  }

  addr = mmap(NULL, mem_size, PROT_READ , MAP_SHARED, fd, 0);
  if (addr == MAP_FAILED) {
    perror("Fail to open shared memory");
    exit(EXIT_FAILURE);
  }

  char buf[mem_size];
  memcpy(buf, addr, mem_size);
  printf("Receive msg from writer: %s\n", buf);

  return 0;
}
