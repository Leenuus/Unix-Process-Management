// This is a client make use of various IPC technique
// to control the simple daemon
#define _XOPEN_SOURCE

#include <signal.h>
#define BUF_SIZE 1024
#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const char DAEMON_PID_PATH[] = "/tmp/my-daemon/pid";
const char DAEMON_LOG[] = "/tmp/my-daemon/log";
// FILE *log_file = fopen(DAEMON_LOG, "r");

int kill_daemon(void);

int main(int argc, char **argv) {

  int opt, status;
  if (argc == 1) {
    fprintf(stderr, "Usage: %s -k kill daemon", argv[0]);
    exit(EXIT_FAILURE);
  }
  while ((opt = getopt(argc, argv, "kh")) != -1) {
    switch (opt) {
    case 'k':
      status = kill_daemon();
      break;
    case 'h':
      printf("Usage: %s -k kill daemon", argv[0]);
      exit(EXIT_SUCCESS);
    default:
      fprintf(stderr, "Usage: %s -k kill daemon", argv[0]);
      exit(EXIT_FAILURE);
    }
  }

  exit(status);
}

int kill_daemon() {

  FILE *pid_file = fopen(DAEMON_PID_PATH, "r");
  char buf[BUF_SIZE];
  int pid;
  if (pid_file == NULL) {
    if (errno == ENOENT) {
      fprintf(stderr, "Daemon PID file not found\nDo you launch the daemon?");
      return 1;
    } else {
      perror("Fail to get PID file");
      return 1;
    }
  } else {
    if (fread(&buf, sizeof(char), BUF_SIZE, pid_file) == -1) {
      fprintf(stderr,
              "Fail to open and read daemon PID file, check you privilege");
      return 1;
    }
    // FIXME
    pid = atoi(buf);
    printf("Daemon PID: %d", pid);
    if (kill(pid, SIGTERM) == -1) {
      perror("Fail to kill daemon");
      return 1;
    }
  }
  return 0;
}
