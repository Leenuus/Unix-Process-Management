#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#define mem_size 1024
const char share_mem_name[] = "/my_mem";

int main()

{

  char *addr;
  int fd;
  fd = shm_open(share_mem_name, O_RDWR | O_CREAT, 0600);

  if (fd == -1) {
    perror("Fail to open shared memory");
    exit(EXIT_FAILURE);
  }

  if (ftruncate(fd, mem_size) == -1) {
    perror("Fail to truncate");
    exit(EXIT_FAILURE);
  }

  addr = mmap(NULL, mem_size, PROT_WRITE, MAP_SHARED, fd, 0);
  if (addr == MAP_FAILED) {
    perror("Fail to map");
    exit(EXIT_FAILURE);
  }

  char src[] = "Hello from share mem";
  memcpy(addr, src, strlen(src));

  getchar();
  munmap(addr, mem_size);
  shm_unlink(share_mem_name);

  return 0;
}
