#! /bin/fish

set opt (fish_opt -s s -l simple)
set opt $opt (fish_opt -s d -l daemon)
argparse $opt -- $argv

function report
    set_color blue
    echo -e $argv[2]
    set_color normal
    fish -c "$argv[1]"
    echo
end

set daemon_dir /tmp/my-daemon/
set d_log "$daemon_dir/log"
set exe ./target/daemon
set service ./my-daemon.service

make
if test -n "$_flag_d"
    # deamon test
    sudo install $exe /usr/local/bin/my-daemon
    sudo cp $service /etc/systemd/system/
    sudo systemctl daemon-reload
    sudo systemctl restart my-daemon
    sudo systemctl status my-daemon
    report "cat $daemon_dir/pid" "Daemon PID:"
    report "tail $d_log" "Daemon Log:"
    report "ls -alh /tmp/my-daemon/" "Permission:"

    report "" "CLEANING: "
    sudo systemctl stop my-daemon
    report "tail $d_log" "Daemon Log:"
    report "ls -alh /tmp/my-daemon/" "Permission:"
    sudo rm -rf $daemon_dir
else
    # simple test
    # sudo install $exe /usr/local/bin/my-daemon
    # sudo cp $service /etc/systemd/system/
    # sudo systemctl daemon-reload
    # sudo systemctl restart my-daemon
    # sudo systemctl status my-daemon
    $exe
    set d_pid (cat "$daemon_dir/pid")
    report '' 'PID: $d_pid'
    report "tail $d_log" "Daemon Log:"
    report "ls -alh /tmp/my-daemon/" "Permission:"

    report "kill -USR1 $d_pid" 'send sig USR1'
    report "kill -USR2 $d_pid" 'send sig USR2'
    report "kill -TERM $d_pid" 'send sig TERM'
    report "tail $d_log" "Daemon Log:"
    report "ls -alh /tmp/my-daemon/" "Permission:"
end
