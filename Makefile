cc ?= gcc
cflags := -Wall -g
target ?= ./target

orphan := $(target)/orphan
orphan-src := orphan.c

daemon := $(target)/daemon
daemon-src := daemon.c

client := $(target)/client
client-src := client.c

pipe := $(target)/pipe
pipe-src := pipe-ipc.c

fifo := $(target)/fifo
fifo-src := fifo-ipc.c

message-queue := $(target)/message-queue
message-queue-src := message-queue.c

share-mem := $(target)/share-mem
share-mem-src := share-mem-related.c

shm_writer := $(target)/shm_writer
shm_writer-src := shm_writer.c

shm_reader := $(target)/shm_reader
shm_reader-src := shm_reader.c

main: $(orphan) $(daemon) $(client) $(pipe) $(share-mem) $(message-queue)  $(fifo) $(shm_reader) $(shm_writer)

$(orphan): $(orphan-src) $(target)
	$(cc) -o $(orphan) $(orphan-src) $(cflags)

$(daemon): $(daemon-src) $(target)
	$(cc) -o $(daemon) $(daemon-src) $(cflags)

$(client): $(client-src) $(target)
	$(cc) -o $(client) $(client-src) $(cflags)

$(pipe): $(pipe-src) $(target)
	$(cc) -o $(pipe) $(pipe-src) $(cflags)

$(fifo): $(fifo-src) $(target)
	$(cc) -o $(fifo) $(fifo-src) $(cflags)

$(message-queue): $(message-queue-src) $(target)
	$(cc) -o $(message-queue) $(message-queue-src) $(cflags)

$(share-mem): $(share-mem-src) $(target)
	$(cc) -o $(share-mem) $(share-mem-src) $(cflags)

$(shm_writer): $(shm_writer-src) $(target)
	$(cc) -o $(shm_writer) $(shm_writer-src) $(cflags)

$(shm_reader): $(shm_reader-src) $(target)
	$(cc) -o $(shm_reader) $(shm_reader-src) $(cflags)

$(target):
	mkdir $(target)

clean: $(target)
	rm -rf $(target)
