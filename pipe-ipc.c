#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BUF_SIZE 1024

int main() {
  int fd[2] = {0};
  int pid;
  if (pipe(fd) == -1) {
    perror("Fail to create pipe");
    exit(EXIT_FAILURE);
  }

  if ((pid = fork()) == -1) {
    perror("Failt to fork");
    exit(EXIT_FAILURE);
  }
  if (pid == 0) {
    // child
    // close write
    close(fd[1]);
    int n;
    char buf[BUF_SIZE];
    while (1) {
      // PITFALLS: strlen(buf) can be any number
      // no gurantee that we can read a exact single chunk
      // of another end's one-time write
      // so the read side is responsible for
      // checking whether the data is enough for use
      int length = strlen("Data Chunk 1");
      int i = 0;
      while (i < length) {
        n = read(fd[0], buf + i, length - i);
        if (n == -1) {
          perror("Fail to read from pipe");
          exit(EXIT_FAILURE);
        } else if (n == 0) {
          printf("\033[%dm Child: EOF receive\033[m\n", 44);
          exit(EXIT_SUCCESS);
        } else {
          i += n;
        }
      }
      i = 0;
      printf("\033[%dm Child receive: %s\033[m \n", 44, buf);
    }
  } else if (pid > 0) {
    // Parent
    // close read
    close(fd[0]);

    int n;
    char buf[BUF_SIZE];
    for (int i = 0; i < 10; i++) {
      sprintf(buf, "Data Chunk %d", i);
      int length = strlen(buf);
      n = write(fd[1], buf, strlen(buf));
      if (n == -1) {
        perror("Parent: Fail to write from pipe");
        exit(EXIT_FAILURE);
      } else if (n == 0) {
        printf("Parent: Pipe close by other side");
        exit(EXIT_SUCCESS);
      }
    }
  }

  return 0;
}
