/*
 *
 * this is a daemon create logs and record its `pid` in /tmp/my-daemon/
 * it writes a entry in the log every 5s
 * when receiving some signals, it ignore and log it
 * when receiving some common used interrupt signals, it exit and log it
 */

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

// TODO singleton

const char *DAEMON_DIR = "/tmp/my-daemon";
const char *PID_FILE_PATH = "/tmp/my-daemon/pid";
int PID_FILE_FD;

char *LOG_PATH = "/tmp/my-daemon/log";
int LOG_FD;

void sig_handler(int sig);

int main() {
  int status = EXIT_SUCCESS;
  pid_t pid;
  struct stat dir_stat;

  if ((status = stat(DAEMON_DIR, &dir_stat) == -1)) {
    // dir no exist
    if (errno != ENOENT) {
      perror("Fail to get file status");
      exit(EXIT_FAILURE);
    } else {
      // anyone can read, write, access the dir
      if (mkdir(DAEMON_DIR, S_IRWXU | S_IRWXO | S_IRWXG) == -1) {
        perror("Fail to create daemon dir");
        exit(EXIT_FAILURE);
      }
    }
  }
  if (S_ISREG(dir_stat.st_mode)) {
    if (unlink(DAEMON_DIR) == -1) {
      perror("Fail to delete file to create daemon dir");
      exit(EXIT_FAILURE);
    }
    // anyone can read, write, access the dir
    if (mkdir(DAEMON_DIR, S_IRWXU | S_IRWXO | S_IRWXG) == -1) {
      perror("Fail to create daemon dir");
      exit(EXIT_FAILURE);
    }
  }

  PID_FILE_FD =
      open(PID_FILE_PATH, O_RDWR | O_CREAT,
           // anyone can read the pid, only user can write the pid file
           S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  LOG_FD = open(LOG_PATH, O_RDWR | O_APPEND | O_CREAT,
                // anyone can read or write the log
                S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);

  if (PID_FILE_FD == -1 || LOG_FD == -1) {
    perror("Fail to open file");
    exit(EXIT_FAILURE);
  }

  if ((pid = fork()) == -1) {
    perror("Fail to fork");
    return EXIT_FAILURE;
  }

  if (pid > 0) {
    exit(0);
  }

  pid_t gid = setsid();

  if (gid == -1) {
    perror("Fail to get new session");
    exit(EXIT_FAILURE);
  }

  if ((pid = fork()) == -1) {
    perror("Fail to fork");
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {

    // parent process write the pid
    char buf[100];
    sprintf(buf, "%d", pid);
    if (write(PID_FILE_FD, buf, strlen(buf)) == -1) {
      perror("Fail to write pid");
      exit(EXIT_FAILURE);
    }
    exit(EXIT_SUCCESS);

  } else {

    // daemon logic
    umask(002);
    chdir("/");
    close(STDOUT_FILENO);
    close(STDIN_FILENO);
    close(STDERR_FILENO);
    open("/dev/null", O_RDONLY);
    open("/dev/null", O_WRONLY);
    open("/dev/null", O_RDWR);

    // signal handling

    struct sigaction sa;
    sa.sa_handler = sig_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;

    sigaction(SIGTERM, &sa, NULL);

    // The signals SIGKILL and SIGSTOP cannot be caught, blocked, or ignored.
    // sigaction(SIGKILL, &sa, NULL);
    // sigaction(SIGSTOP, &sa, NULL);

    sigaction(SIGABRT, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGUSR1, &sa, NULL);
    sigaction(SIGUSR2, &sa, NULL);

    time_t t;
    char buf[100];
    sprintf(buf, "Daemon START: %s", ctime(&t));
    if (write(LOG_FD, buf, strlen(buf)) == -1) {
      perror("Fail to write start log");
      exit(EXIT_FAILURE);
    }
    while (true) {
      time(&t);
      sprintf(buf, "Daemon Msg: %s", ctime(&t));
      if (write(LOG_FD, buf, strlen(buf)) == -1) {
        perror("Fail to write log");
        exit(EXIT_FAILURE);
      }
      sleep(5);
    }
  }

  return EXIT_SUCCESS;
}

void sig_handler(int sig) {
  char buf[1024];
  time_t t;
  time(&t);
  char *sig_str = strsignal(sig);
  int status = EXIT_SUCCESS;
  if (sig == SIGTERM || sig == SIGINT) {
    if (unlink(PID_FILE_PATH) == -1) {
      sprintf(buf, "Fail to unlink pid file: %s", ctime(&t));
      status = EXIT_FAILURE;
      if (write(LOG_FD, buf, strlen(buf)) == -1) {
        perror("Fail to write log");
        status = EXIT_FAILURE;
      }
    }
    sprintf(buf, "Exit at signal %s: %s", sig_str, ctime(&t));
    if (write(LOG_FD, buf, strlen(buf)) == -1) {
      perror("Fail to write log");
      status = EXIT_FAILURE;
    }
    exit(status);

  } else {

    sprintf(buf, "Receive signal %s %s", sig_str, ctime(&t));
    if (write(LOG_FD, buf, strlen(buf)) == -1) {
      perror("Fail to write log");
      exit(EXIT_FAILURE);
    }
  }
}
