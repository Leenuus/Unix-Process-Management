#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#define DATASIZE 128

int main() {

  char *addr;
  addr = mmap(NULL, DATASIZE, PROT_WRITE | PROT_READ,
              MAP_SHARED | MAP_ANONYMOUS, -1, 0);
  if (addr == MAP_FAILED) {
    perror("Fail to share memory");
    exit(EXIT_FAILURE);
  }

  pid_t pid;
  if ((pid = fork()) == -1) {
    perror("Fail to fork");
    exit(EXIT_FAILURE);
  }

  char parent_msg[] = "Hello from parent!";
  int p_length = strlen(parent_msg);
  char child_msg[] = "Hello from child!";
  int c_length = strlen(child_msg);
  if (pid > 0) {
    // parent
    memcpy(addr + c_length + 1 + 1, parent_msg, p_length);
    sleep(1);
    printf("Parent receive Msg from child: %s\n", addr);
  } else if (pid == 0) {
    // child
    memcpy(addr, child_msg, c_length);
    sleep(1);
    printf("Child receives Msg from parent: %s\n", addr + c_length + 1 + 1);
  }
}
