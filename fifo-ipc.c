#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define _XOPEN_SOURCE_ 700
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

const char *FIFO_PATH = "/tmp/my-fifo";
#define BUF_SIZE 1024
char buf[BUF_SIZE] = {0};

int cleanup(void);

int main() {

  if (mkfifo(FIFO_PATH, 0644) == -1) {
    if (errno == EEXIST) {
      printf("FIFO already exists\n");
    } else {
      perror("Fail to create fifo");
      exit(EXIT_FAILURE);
    }
  }

  int fd;
  if ((fd = open(FIFO_PATH, O_WRONLY)) == -1) {
    perror("Fail to open fifo");
    cleanup();
    exit(EXIT_FAILURE);
  }

  if (sprintf(buf, "Hello from FIFO creator") == -1) {
    perror("Fail to write buffer");
    cleanup();
    exit(EXIT_FAILURE);
  }

  int n = 0;
  int length = strlen(buf);
  while (n < length) {
    int i = write(fd, buf, length);
    if (i == -1) {
      perror("Fail to write to FIFO");
      cleanup();
      exit(EXIT_FAILURE);
    } else if (i == 0) {
      printf("FIFO closed");
      exit(EXIT_SUCCESS);
    } else {
      n += i;
    }
  }
  return -cleanup();
}

int cleanup(void) {
  if (unlink(FIFO_PATH) == -1) {
    perror("Fail to unlink fifo");
    return -1;
  } else {
    return 0;
  }
}
